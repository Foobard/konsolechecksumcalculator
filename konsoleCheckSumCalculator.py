#! /usr/bin/env python

checkSumIds = ['md5', 'sha1', 'sha224',
               'sha256', 'sha384', 'sha512']


def parseArguments():
    parser = argparse.ArgumentParser(allow_abbrev=False)

    parser.add_argument('-f', '--fpath',
                        nargs=1,
                        type=str,
                        required=True,
                        help='file to process')

    parser.add_argument('-e', '--evaluate',
                        nargs='+',
                        type=str,
                        choices=checkSumIds,
                        help='a list of sums to evaluate')

    for checkSumId in checkSumIds:
        parser.add_argument('--' + checkSumId,
                            nargs=1,
                            type=str,
                            help=checkSumId + 'sum')

    return vars(parser.parse_args())


def getSums(checkSumIds, path_to_file):
    hashFunctions = [getattr(hashlib, checkSumId)() for checkSumId
                     in checkSumIds]
    with open(path_to_file, 'rb') as f:
        for chunk in iter(lambda: f.read(4096), b""):
            [hashFunction.update(chunk) for hashFunction in hashFunctions]
    checkSumValues = [hashFunction.hexdigest() for hashFunction
                      in hashFunctions]
    return zip(checkSumIds, checkSumValues)


def printResults(pargs):
    clCheckSumIds = [k for k, v in pargs.items() if v and k in checkSumIds]
    if pargs['evaluate']:
        if clCheckSumIds:
            sys.exit("Error: arguments '{0}' and '{1}' are not compatible"
                     .format('--evaluate', clCheckSumIds))
        else:
            checkSumValues = getSums(pargs['evaluate'], *pargs['fpath'])
            for hashSumValue in checkSumValues:
                print('{0}: {1}  {2}'.format(*hashSumValue, *pargs['fpath']))

    else:
        checkSumValues = dict(getSums(clCheckSumIds, *pargs['fpath']))
        for clCheckSumId in clCheckSumIds:
            if pargs[clCheckSumId][0] == checkSumValues[clCheckSumId]:
                print('{0}s matched'.format(clCheckSumId))

            else:
                print('{0}s not matched'.format(clCheckSumId))


if __name__ == "__main__":
    import argparse
    import hashlib
    import sys

    printResults(parseArguments())
